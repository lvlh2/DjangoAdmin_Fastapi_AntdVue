# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from datetime import datetime

from fastapi.logger import logger
from sqlalchemy import and_
from werkzeug.datastructures.structures import MultiDict

from apps.constants.message import PAGE_LIMIT
from apps.forms.item_cate import ItemCateForm
from apps.models.item_cate import ItemCate
from apps.services import item
from extends import db
from utils import R, regular
from utils.utils import getImageURL, uid, saveImage


# 查询栏目数据
async def ItemCateList(request):
    try:
        # 页码
        page = int(request.query_params.get("page", 1))
        # 每页数
        limit = int(request.query_params.get("limit", PAGE_LIMIT))
        # 实例化查询对象
        query = db.query(ItemCate).filter(ItemCate.is_delete == 0)
        # 栏目名称
        name = request.query_params.get('name')
        if name:
            query = query.filter(ItemCate.name.like('%' + name + '%'))
        # 栏目状态筛选
        status = request.query_params.get('status')
        if status:
            query = query.filter(ItemCate.status == status)
        # 排序
        query = query.order_by(ItemCate.sort.asc())
        # 记录总数
        count = query.count()
        # 分页查询
        list = query.limit(limit).offset((page - 1) * limit).all()
        # 关闭连接
        # 实例化结果
        result = []
        # 遍历数据源
        if len(list) > 0:
            for v in list:
                # 对象转字典
                data = v.to_dict()
                # 栏目图片
                data['cover'] = getImageURL(v.cover) if v.cover else None
                # 查询站点信息
                item_info = await item.ItemDetail(v.id)
                if item_info:
                    data['item_name'] = item_info.get('name') if item_info else None
                # 加入列表
                result.append(data)
        # 返回结果
        return R.ok(data=result, count=count)
    except:
        # 抛出异常
        logger.error('运行异常')
        raise
    finally:
        # 关闭连接
        db.close()


# 根据栏目ID查询详情
async def ItemCateDetail(itemcate_id):
    try:
        # 根据ID查询栏目
        cate = db.query(ItemCate).filter(and_(ItemCate.id == itemcate_id, ItemCate.is_delete == 0)).first()
        # 查询结果判空
        if not cate:
            return None
        # 对象转字典
        data = cate.to_dict()
        # 栏目图片
        if cate.cover:
            data['cover'] = getImageURL(cate.cover)
        # 返回结果
        return data
    except:
        # 抛出异常
        logger.error('运行异常')
        raise
    finally:
        # 关闭连接
        db.close()


# 添加栏目
async def ItemCateAdd(request):
    # 获取请求参数
    json_data = await request.json()
    # 表单验证
    form = ItemCateForm(MultiDict(json_data))
    if not form.validate():
        # 获取错误描述
        err_msg = regular.get_err(form)
        # 返回错误信息
        return R.failed(msg=err_msg)

    # 是否有封面
    is_cover = form.is_cover.data
    if is_cover == 1:
        # 栏目图片
        cover = form.cover.data
        # 图片处理
        if cover:
            form.cover.data = saveImage(cover, "item_cate")
        else:
            form.cover.data = None
    else:
        form.cover.data = None

    # 表单数据赋值给对象
    cate = ItemCate(**form.data)
    cate.create_user = uid(request)
    # 插入数据
    cate.save()
    # 返回结果
    return R.ok(msg="添加成功")


# 更新栏目
async def ItemCateUpdate(request):
    # 获取请求参数
    json_data = await request.json()
    # 表单验证
    form = ItemCateForm(MultiDict(json_data))
    if not form.validate():
        # 获取错误描述
        err_msg = regular.get_err(form)
        # 返回错误信息
        return R.failed(msg=err_msg)

    # 记录ID判空
    id = form.data['id']
    if not id or int(id) <= 0:
        return R.failed("记录ID不能为空")

    try:
        # 根据ID查询记录
        cate = db.query(ItemCate).filter(and_(ItemCate.id == id, ItemCate.is_delete == 0)).first()
        # 查询结果判空
        if not cate:
            return R.failed("记录不存在")
        # 是否有封面
        is_cover = form.is_cover.data
        if is_cover == 1:
            # 栏目图片
            cover = form.cover.data
            # 图片处理
            if cover:
                form.cover.data = saveImage(cover, "item_cate")
            else:
                form.cover.data = None
        # 删除ID元素
        del form['id']
        updData = form.data
        updData['update_user'] = uid(request)
        updData['update_time'] = datetime.now()
        # 更新数据
        result = db.query(ItemCate).filter_by(id=id).update(updData)
        # 提交数据
        db.commit()
        if not result:
            return R.failed("更新失败")
        # 返回结果
        return R.ok(msg="更新成功")
    except:
        # 事务回滚
        db.rollback()
        raise
    finally:
        # 关闭连接
        db.close()


# 删除栏目
async def ItemCateDelete(cate_id):
    # 记录ID为空判断
    if not cate_id:
        return R.failed("记录ID不存在")
    try:
        # 分裂字符串
        list = cate_id.split(',')
        # 计数器
        count = 0
        # 遍历数据源
        if len(list) > 0:
            for vId in list:
                # 根据ID查询记录
                cate = db.query(ItemCate).filter(and_(ItemCate.id == int(vId), ItemCate.is_delete == 0)).first()
                # 查询结果判空
                if not cate:
                    return R.failed("记录不存在")
                # 设置删除标识
                cate.is_delete = 1
                # 提交数据
                db.commit()
                # 计数器+1
                count += 1
        # 返回结果
        return R.ok(msg="本次共删除{0}条数据".format(count))
    except:
        # 事务回滚
        db.rollback()
        raise
    finally:
        # 关闭连接
        db.close()


# 获取栏目列表
async def GetCateList():
    try:
        list = db.query(ItemCate).filter(and_(ItemCate.is_delete == 0, ItemCate.status == 1)).order_by(
            ItemCate.sort.asc()).all()
        # 实例化栏目列表
        cate_list = []
        # 遍历数据源
        for v in list:
            # 对象转字典
            item = v.to_dict()
            # 加入列表
            cate_list.append(item)
        # 返回结果
        return cate_list
    except:
        # 抛出异常
        logger.error('运行异常')
        raise
    finally:
        # 关闭连接
        db.close()
