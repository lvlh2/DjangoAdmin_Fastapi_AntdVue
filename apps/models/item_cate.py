# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

from sqlalchemy import Column, String, Integer

from apps.models.base_db import base_db
from apps.models.base_model import base_model
from config.env import DB_PREFIX


# 栏目模型
class ItemCate(base_model, base_db):
    # 设置表名
    __tablename__ = DB_PREFIX + "item_cate"
    # 栏目名称
    name = Column(String(150), nullable=False, comment="栏目名称")
    # 上级ID
    pid = Column(Integer, default=0, comment="上级ID")
    # 站点ID
    item_id = Column(Integer, default=0, comment="站点ID")
    # 拼音(全拼)
    pinyin = Column(String(150), nullable=False, comment="拼音(全拼)")
    # 拼音(简拼)
    code = Column(String(150), nullable=False, comment="拼音(简拼)")
    # 是否有封面：1-是 2-否
    is_cover = Column(Integer, default=2, comment="是否有封面：1-是 2-否")
    # 封面地址
    cover = Column(String(255), nullable=False, comment="封面地址")
    # 栏目状态：1-正常 2-停用
    status = Column(Integer, default=1, comment="栏目状态：1-正常 2-停用")
    # 栏目排序
    sort = Column(Integer, default=0, comment="栏目排序")
    # 栏目备注
    note = Column(String(255), nullable=True, comment="栏目备注")

    # 初始化
    def __init__(self, id, name, pid, item_id, pinyin, code, is_cover, cover, status, sort, note):
        self.id = id
        self.name = name
        self.pid = pid
        self.item_id = item_id
        self.pinyin = pinyin
        self.code = code
        self.is_cover = is_cover
        self.cover = cover
        self.status = status
        self.sort = sort
        self.note = note

    def __str__(self):
        return "栏目{}".format(self.name)
